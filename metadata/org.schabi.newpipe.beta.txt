AntiFeatures:NonFreeNet
Categories:Multimedia,Internet
License:GPL-3.0+
Web Site:https://newpipe.schabi.org
Source Code:https://github.com/TeamNewPipe/NewPipe
Issue Tracker:https://github.com/TeamNewPipe/NewPipe/issues
Changelog:https://github.com/TeamNewPipe/NewPipe/releases
LiberapayID:34969
Bitcoin:16A9J59ahMRqkLSZjhYj33n9j3fMztFxnh

Auto Name:NewPipe Beta
Summary:Lightweight Mediaservices app
Description:
Lightweight frontend for several audio and video platforms such as YouTube,
SoundCloud, etc. NewPipe comes without proprietary bindings like Google play
services, and it sends as few data as possible. Also it has several other
awesome features like Downloading and listening to videos.

This a very early version of the app, so not all functionality is implemented,
and there may still be a lot of bugs. But all in all it's doing what it is
supposed to do.

ATTENTION: This is the beta version of the app. It has embedded the newest
features, and functions meant to be part of NewPipe. However this is a highly
unstable app, and only thought for testing, so use at your own risk. If you want
to have a more reliable version of this app please use our
[https://f-droid.org/packages/org.schabi.newpipe/ release candidate].
.

Repo Type:git
Repo:https://github.com/TeamNewPipe/NewPipe

Build:0.11.6,47
    commit=v0.11.6-beta
    subdir=app
    output=build/outputs/apk/beta/*.apk
    build=gradle assembleBeta

Build:0.12.0,48
    commit=v0.12.0-beta
    subdir=app
    output=build/outputs/apk/beta/*.apk
    build=gradle assembleBeta

Auto Update Mode:Version v%v
Update Check Mode:Tags .*-beta$
Current Version:0.12.0
Current Version Code:48
